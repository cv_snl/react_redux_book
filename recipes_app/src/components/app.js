import React, { Component } from 'react';
import Menu from './menu'
import data from '../stabs'

class App extends Component {
  render() {
    return (
      <Menu title="Delicious Recipes" recipes={ data } />
    )
  }
}

export default App;