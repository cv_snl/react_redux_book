import React from 'react';
import Instructions from './instructions'
import IngredientsList from './ingredients_list'
import Summary from './summary'

const Recipe = ({ name, ingredients, steps }) => (
  <section>
    <h1>{ name }</h1>
    <IngredientsList list={ ingredients } />
    <Instructions title="Cooking Instructions" steps={ steps } />
    <Summary title={ name }
             ingredients={ ingredients.length }
             steps={ steps.length }/>
  </section>
)

export default Recipe